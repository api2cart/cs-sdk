C# SDK for API2Cart

# What is API2Cart
<b>With API2Cart shopping platform
integration is easy.</b>

<p align="left">
  <img src="https://api2cart.com/wp-content/uploads/2018/07/scheme-for-adv.jpg">
</p>

Integrate once, save 4-8 weeks and thousands of dollars on each integration. Never worry about maintaining separate connections.

To get started with API2Cart register an account for a 30-day trial. You can add stores and execute methods to test how API2Cart works with orders, products, categories, and other entities from stores.

<h2><b>Webhooks</b></h2>

Subscribe to events your app needs (e.g. product add or order update) and have them delivered right to the specified URL as they happen.

<h2><b>100+ methods to manage data</b></h2>

Retrieve, add, delete, update, and synchronize store data from all or any of the supported shopping carts.

We provide detailed documentation http://docs.api2cart.com to help you connect multiple shopping carts and marketplaces in one go. 

See all supported methods and platforms https://api2cart.com/supported-api-methods/. 

<h2><b>Contact us</b></h2>
  
If you have any questions or problems, please contact us.
You can also reach us at manager@api2cart.com. You can also mail us your ideas and suggestions about any changes.

<h2><b>Support</b></h2>

This API is professionally supported by API2Cart. If you want to discuss details of how API2Cart works, contact our customer success manager at manager@api2cart.com.
